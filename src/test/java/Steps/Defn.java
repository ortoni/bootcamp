package Steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.servicenow.pages.AppCreateHome;
import com.testleaf.servicenow.pages.AppCreateLogin;
import com.testleaf.servicenow.pages.AppCreateSubmit;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Defn extends SeleniumBase {
	public Defn()
	{
		PageFactory.initElements(getDriver(), this);
	}
	
@FindBy(id="gsft_main") WebElement elebtnFrame;
@FindBy(id="user_name") WebElement eletxtUsername;
@FindBy(id="user_password") WebElement eletxtPassword;
@FindBy(id="sysverb_login") WebElement elebtnLogin;
@FindBy(id="filter") WebElement eletxtfilter;
@FindBy(xpath="(//div[text()='My Work'])[1]") WebElement elebtnwork;
@FindBy(xpath="//button[text()='New']") WebElement elebtnNew;
@FindBy(xpath="//a[text()='Group approval']") WebElement elebtnGroup;
@FindBy(linkText="Group approval") WebElement elelnkGroup;
@FindBy(id="sysapproval_group.short_description") WebElement eletxtshortdesc;;
@FindBy(id="sysapproval_group.description") WebElement eletxtdesc;
@FindBy(xpath="(//button[@id='sysverb_insert'])[1]") WebElement elebtnsub;

@Given("Enter the Username as (.*)")
public AppCreateLogin enterusername(String data)
{
	startApp("chrome","https://dev87059.service-now.com");
	switchToFrame(0);	
	clearAndType(eletxtUsername, data);
	return this;
}

@And("Enter the password as (.*)")
public AppCreateLogin enterpassword(String data)
{
	clearAndType(eletxtPassword,data);
	return this;
}

@When("Click on the login button")
public AppCreateHome clickLogin()
{
	click(elebtnLogin);
	return new AppCreateHome();
}

@Then("Enter details in textbox")
public AppCreateHome entertext(String data)
{
	clearAndType(eletxtfilter,data);
	return this;
}
@When("Click MyWork button")
public AppCreateHome clicktext()
{
	click(elebtnwork);
	return this;
}

@And("Click New button")
public AppCreateHome clickNew()
{
	click(elebtnNew);
	return this;
}

@When("Click Group Approval")
public AppCreateSubmit clickgroupapp()
{   	
	click(elelnkGroup);
	return new AppCreateSubmit();
}
@Given("Enter short desc as <short desc>")
public AppCreateSubmit entershort(String data)
{   
	clearAndType(eletxtshortdesc,data);
	return this;
}	
@And("Enter desc as <desc>")
public AppCreateSubmit enterdesc(String data)
{   
	clearAndType(eletxtdesc,data);
	click(elebtnsub);	
	return this;
}
@When("Click Submit button")
public AppCreateSubmit clicksub()
{   
	click(elebtnsub);	
	return this;
}
	
}
