package com.testleaf.servicenow.testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.servicenow.pages.Problem_Management_Page;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class VerifyProblem_Update extends ProjectSpecificMethods {
	

	
	@Test(priority=1)
	public void VerifyCreateProblem() throws IOException, InterruptedException
	{
			
		new Problem_Management_Page()
		.login()
		.createProblem();
		
	}
	
	@Test(priority=2)
	public void VerifyUpdateProblem() throws IOException, InterruptedException
	{
			
		new Problem_Management_Page()
		.login()
		.searchProblem()
		.updateProblem();	
		
	}
	
	@Test(priority=3)
	public void VerifyAssignProblem() throws IOException, InterruptedException
	{
			
		new Problem_Management_Page()
		.login()
		.searchProblem()
		.AssignProblem();
		
	}

	
	@Test(priority=4)
	public void VerifyDeleteProblem() throws IOException, InterruptedException
	{
			
		new Problem_Management_Page()
		.login()
		.searchProblem()
		.DeleteProblem();
		
	}

	
	
	

}
