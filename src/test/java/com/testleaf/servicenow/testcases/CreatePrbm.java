package com.testleaf.servicenow.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.testleaf.servicenow.pages.PrbmMgmtCreate;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class CreatePrbm extends ProjectSpecificMethods{
	  @BeforeTest
	   public void beforetest()
	   {
			testcaseName="TC_01";
			testcaseDec="Create Problem";
			author="Jai"; 
			category="sanity";
	   }
	    @Test
	    public void VrfyCreatePrbm() throws IOException, InterruptedException
		{				
			new PrbmMgmtCreate()
			.login()
            .enter()
			.submit();
		}
}
