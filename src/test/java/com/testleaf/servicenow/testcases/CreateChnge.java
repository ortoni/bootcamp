package com.testleaf.servicenow.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

import com.testleaf.servicenow.pages.ChngeMgmtAssess;
import com.testleaf.servicenow.pages.ChngeMgmtCreate;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class CreateChnge extends ProjectSpecificMethods{

       @BeforeTest
	   public void beforetest()
	   {
			testcaseName="TC_001";
			testcaseDec="Create Change";
			author="Jai"; 
			category="smoke";
	   }
	  
	    public void VrfyAssessChnge() throws IOException, InterruptedException
		{				
			new ChngeMgmtCreate()
			.login()
            .enter()
			.verify();
		}	
}
