package com.testleaf.servicenow.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.testleaf.servicenow.pages.AppCreateLogin;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class AppCreateTC extends ProjectSpecificMethods{
	
	@BeforeTest
	public void setData() {
		testcaseName = "Create new group Approval";
		testcaseDec = "Create Flow";
		author = "Jayashri";
		category = "Sanity";
		//excelFileName = "TC001";
		}
}
	
	/*@Test
	public void CreateNewGroupApp(String uname,String pswd,String txt,String sdesc,String desc)
	{
		new AppCreateLogin()
		.enterusername(uname)
		.enterpassword(pswd)
		.clickLogin()
		.entertext(txt)
		.clicktext()
		.clickNew()
		.clickgroupapp()
		.entershort(sdesc)
		.enterdesc(desc)
		.clicksub();
	}*/


