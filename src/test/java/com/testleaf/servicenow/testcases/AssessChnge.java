package com.testleaf.servicenow.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import org.testng.annotations.BeforeTest;

import com.testleaf.servicenow.pages.ChngeMgmtAssess;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

public class AssessChnge extends ProjectSpecificMethods{

	  @BeforeTest
	   public void beforetest()
	   {
			testcaseName="TC_002";
			testcaseDec="Assess Change";
			author="Jai"; 
			category="smoke";
	   }
	  
	    public void VrfyAssessChnge() throws IOException, InterruptedException
		{				
			new ChngeMgmtAssess()
			.login()
            .search("CHG0030010")
			.update();
		}	

}
