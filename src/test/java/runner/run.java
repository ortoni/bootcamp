package runner;

import org.testng.annotations.BeforeTest;

import com.testleaf.testng.api.base.ProjectSpecificMethods;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/feature/Featfile.feature",glue= {"com.testleaf"},
		monochrome=true)

public class run extends ProjectSpecificMethods{

	@BeforeTest
	public void setData() {
		testcaseName = "Create new group Approval";
		testcaseDec = "TC001_Positive Create Flow ";
		author = "Jayashri";
		category = "Smoke";
	}
}
