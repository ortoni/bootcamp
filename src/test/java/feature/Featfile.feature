Feature: Create new group Approval for ServiceNow Application

Scenario Outline: TC001 Positive Create Flow 
Given Enter the Username as <username>
And Enter the password as <password>
When Click on the login button
Then Enter details in textbox as <filter>
When Click MyWork button
And Click New button 
When Click Group Approval
Given Enter short desc as <shortdesc>
And Enter desc as <desc>
When Click Submit button

Examples:
|username|password|filter|shortdesc|desc|
|admin|India@123|My Work|Issue with networking|Testing|


