package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;

public class ChngeMgmtCreate extends SeleniumBase {
	public ChngeMgmtCreate()
	{
		PageFactory.initElements(getDriver(), this);
	}
	
	@FindBy(id="user_name") WebElement eletxtUsername;
	@FindBy(id="user_password") WebElement eletxtPassword;
	@FindBy(id="sysverb_login") WebElement elebtnLogin;
	@FindBy(xpath="//input[@placeholder='Filter navigator']") WebElement elebtnChange;
	@FindBy(xpath="//span[text()='Change']") WebElement elebtntarget;
	@FindBy(xpath="(//div[text()='Create New'])[3]") WebElement elebtnCreate;
	@FindBy(id="gsft_main") WebElement elebtnFrame;
	@FindBy(linkText="Normal: Changes without predefined plans that require approval and CAB authorization.") WebElement elelinkNormal;
	@FindBy(id="change_request.number") WebElement eletcxttest;
	@FindBy(xpath="(//input[@class='form-control'])[1]") WebElement eletxtsend1;
	@FindBy(xpath="(//button[@name='not_important'])[2]") WebElement elebtnclk1;
	@FindBy(xpath="(//input[@placeholder='Search'])[1]") WebElement elebtnsend2;
	@FindBy(xpath="((//table[@id='change_request_table'])[1]/tbody/tr/td/a)[2]") WebElement elebtndisp;


public ChngeMgmtCreate login()
{
	startApp("chrome","https://dev87059.service-now.com");
	switchToFrame(0);
	clearAndType(eletxtUsername, "admin");
	clearAndType(eletxtPassword, "India@123");
	click(elebtnLogin);
	click(elebtnChange);
	click(elebtntarget);
	click(elebtnCreate);
	switchToFrame(elebtnFrame);
	click(elelinkNormal);
	getAttribute(eletcxttest,"value");
	return this;
}

public ChngeMgmtCreate enter()
{
	clearAndType(eletxtsend1,"Bootcamp");
    click(elebtnclk1);
	clearAndType(elebtnsend2,"test");
	pressEnterKey(elebtnsend2);
	return this;
}

public ChngeMgmtCreate verify()
{
    getAttribute(elebtndisp,"result");	
	return this;
}
}




