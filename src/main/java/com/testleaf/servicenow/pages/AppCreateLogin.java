package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class AppCreateLogin extends ProjectSpecificMethods {
public AppCreateLogin() {
	PageFactory.initElements(getDriver(),this);
}

@FindBy(id="gsft_main") WebElement elebtnFrame;
@FindBy(id="user_name") WebElement eletxtUsername;
@FindBy(id="user_password") WebElement eletxtPassword;
@FindBy(id="sysverb_login") WebElement elebtnLogin;
@FindBy(id="filter") WebElement eletxtfilter;
@FindBy(xpath="(//div[text()='My Work'])[1]") WebElement elebtnwork;
@FindBy(xpath="//button[text()='New']") WebElement elebtnNew;
@FindBy(xpath="//a[text()='Group approval']") WebElement elebtnGroup;
@FindBy(linkText="Group approval") WebElement elelnkGroup;
@FindBy(id="sysapproval_group.short_description") WebElement eletxtshortdesc;;
@FindBy(id="sysapproval_group.description") WebElement eletxtdesc;
@FindBy(xpath="(//button[@id='sysverb_insert'])[1]") WebElement elebtnsub;



@Given("Enter the Username as (.*)")
public AppCreateLogin enterusername(String data)
{
	switchToFrame(0);	
	clearAndType(eletxtUsername, data);
	return this;
}

@And("Enter the password as (.*)")
public AppCreateLogin enterpassword(String data)
{
	clearAndType(eletxtPassword,data);
	return this;
}

@When("Click on the login button")
public AppCreateHome clickLogin()
{
	click(elebtnLogin);
	return new AppCreateHome();
}
}
