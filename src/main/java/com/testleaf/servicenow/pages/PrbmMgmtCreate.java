package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;

public class PrbmMgmtCreate extends SeleniumBase{
	public PrbmMgmtCreate()
	{
		PageFactory.initElements(getDriver(), this);
	}
    
	@FindBy(id="gsft_main") WebElement elebtnFra;
	@FindBy(id="user_name") WebElement eletxtUsername;
	@FindBy(id="user_password") WebElement eletxtPassword;
	@FindBy(id="sysverb_login") WebElement elebtnLogin;
	@FindBy(id="filter") WebElement eletxtdetails;
	@FindBy(xpath="(//div[text()='Create New'])[2]") WebElement elebtntar;
	@FindBy(id="lookup.problem.cmdb_ci") WebElement elebtny;
	@FindBy(xpath="//input[@placeholder='Search']") WebElement elebtnc;
	@FindBy(linkText="Oracle FLX") WebElement elelink;
	@FindBy(id="problem.description") WebElement eletxtprbm;
	@FindBy(id="problem.short_description") WebElement eletxtdesc;
	@FindBy(xpath="(//button[text()='Submit'])[1]") WebElement elebtnsub;


	public PrbmMgmtCreate login()
	{
		startApp("chrome","https://dev87059.service-now.com");
		switchToFrame(elebtnFra);
		clearAndType(eletxtUsername, "admin");
		clearAndType(eletxtPassword, "India@123");
		click(elebtnLogin);
		return this;
	}

		public PrbmMgmtCreate enter()
		{
			clearAndType(eletxtdetails, "Problem");
			moveToElement(elebtntar);
			click(elebtntar);
			switchToFrame(0);
			click(elebtny);
			return this;
		}
		
		public PrbmMgmtCreate submit()
		{
			switchToWindow(1);
			clearAndType(elebtnc, "Oracle FLX");
			pressEnterKey(elebtnc);
			click(elelink);
			switchToWindow(0);
			switchToFrame(elebtnFra);
			clearAndType(eletxtprbm, "oracle problem");
			clearAndType(eletxtdesc, "Problems faced");
			click(elebtnsub);
			close();
			return this;
		}
	

}
