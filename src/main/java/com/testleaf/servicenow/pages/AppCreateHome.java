package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AppCreateHome extends ProjectSpecificMethods {
	public AppCreateHome() {
		PageFactory.initElements(getDriver(), this);
	}
@CacheLookup
@FindBy(id="filter") WebElement eletxtfilter;
@FindBy(xpath="(//div[text()='My Work'])[1]") WebElement elebtnwork;
@FindBy(xpath="//button[text()='New']") WebElement elebtnNew;


@Then("Enter details in textbox as (.*)")
public AppCreateHome entertext(String data)
{
	clearAndType(eletxtfilter,data);
	return this;
}
@When("Click MyWork button")
public AppCreateHome clicktext()
{
	click(elebtnwork);
	return this;
}

@And("Click New button")
public AppCreateGroup clickNew()
{
	switchToFrame(0);	
	click(elebtnNew);
	return new AppCreateGroup();
} 
	
}
