package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.testleaf.selenium.api.base.SeleniumBase;

public class Problem_Management_Page extends SeleniumBase {
	
	public static  String  pbmNumber;
	
	public Problem_Management_Page(){
		
		PageFactory.initElements(getDriver(), this);
	}
	//PRB0000002
	
	@FindBy(id="user_name") WebElement eletxtUsername;
	@FindBy(id="user_password") WebElement eletxtPassword;
	@FindBy(id="sysverb_login") WebElement elebtnLogin;
	@FindBy(id="filter") WebElement elebtxtFilter;
	@FindBy(xpath="(//div[text()='Open'])[2]") WebElement elemnuOpen;	
	@FindBy(xpath="//input[@placeholder='Search'][@class='form-control']") WebElement eletxtSearch;
	@FindBy(xpath="//table[@id='problem_table']/tbody/tr//td[3]/a") WebElement elelnkNumber;
	@FindBy(id="problem.impact") WebElement elebddImpact;
	@FindBy(xpath="//select[@id=problem.urgency") WebElement elebddUrgency;
	@FindBy(id="activity-stream-textarea") WebElement eletxtareaNotes;
	@FindBy(id="sysverb_update") WebElement elebtnUpdate;
	@FindBy(id="sysverb_delete") WebElement elebtnDelete;
	@FindBy(id="ok_button") WebElement elebtnDelete_OK;
	
	
	@FindBy(xpath="(//div[text()='Create New'])[2]") WebElement elemnuCreateNew;
	
	@FindBy(id="gsft_main") WebElement elefrmCreateNew;
	
	
	@FindBy(id="lookup.problem.first_reported_by_task") WebElement elelovReportedby;
	
	@FindBy(xpath="(//tr[@data-type='list2_row']/td/a[text()='ITIL User'])[1]") WebElement elelovvalue;
	
	@FindBy(id="problem.category") WebElement eleddcatogory;
	
	@FindBy(id="problem.short_description") WebElement eletxtProblemdesc;
	
	@FindBy(id="sysverb_insert_bottom") WebElement elebtnSubmit;
	
	@FindBy(id="sys_readonly.problem.number") WebElement elepbmNumber;
	
	@FindBy(id="lookup.problem.assigned_to") WebElement elelovAssignedTo;
	
	@FindBy(id="sys_display.problem.assigned_to") WebElement eletxtUser;
	@FindBy(xpath="//a[text()='Problem Manager']") WebElement elelovUser;
		
	public Problem_Management_Page login()
	{
		switchToFrame(0);
		clearAndType(eletxtUsername, "admin");
		clearAndType(eletxtPassword, "India@123");
		click(elebtnLogin);
		return this;
	}
	
	public Problem_Management_Page searchProblem() throws InterruptedException
	{
	
		clearAndType(elebtxtFilter, " problem");	
		pressEnterKey(elebtxtFilter);
		//Thread.sleep(7000);
		click(elemnuOpen);
		switchToFrame(0);
		clearAndType(eletxtSearch, pbmNumber);		
		pressEnterKey(eletxtSearch);
		return this;
	}
	
	public Problem_Management_Page updateProblem()
	{
	
		click(elelnkNumber);
		selectDropDownUsingIndex(elebddImpact,2);
		//selectDropDownUsingIndex(elebddUrgency,2);
		scrolltoPosition("100");
		clearAndType(eletxtareaNotes, "Service Notes1");
		click(elebtnUpdate);
		return this;
		
		
	}
	
	
	public Problem_Management_Page createProblem()
	{
		clearAndType(elebtxtFilter, " problem");	
		pressEnterKey(elebtxtFilter);
		//Thread.sleep(7000);
		click(elemnuCreateNew);
		switchToFrame(elefrmCreateNew);
		click(elelovReportedby);
	
		
		selectDropDownUsingIndex(eleddcatogory,2);

		clearAndType(eletxtProblemdesc, "Service Notes1");
		
		pbmNumber = getAttribute(elepbmNumber, "value");
		
		System.out.println(pbmNumber);
		click(elebtnSubmit);
		return this;
		
		
	}
	

	public Problem_Management_Page AssignProblem()
	{
	
		click(elelnkNumber);		
		clearAndType(eletxtUser, "Problem Manager");		
		click(elebtnUpdate);
		return this;
		
		
	}
	
	public Problem_Management_Page DeleteProblem()
	{
	
		click(elelnkNumber);	
			
		click(elebtnDelete);
		click(elebtnDelete_OK);
		return this;
		
		
	}

}
