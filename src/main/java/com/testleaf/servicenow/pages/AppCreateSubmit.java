package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;
import com.testleaf.testng.api.base.ProjectSpecificMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class AppCreateSubmit extends ProjectSpecificMethods{
	
	public AppCreateSubmit() {
		PageFactory.initElements(getDriver(), this);
	}
@CacheLookup
@FindBy(id="sysapproval_group.short_description") WebElement eletxtshortdesc;;
@FindBy(id="sysapproval_group.description") WebElement eletxtdesc;
@FindBy(xpath="(//button[@id='sysverb_insert'])[1]") WebElement elebtnsub;

@Given("Enter short desc as (.*)")
public AppCreateSubmit entershort(String data)
{   
	clearAndType(eletxtshortdesc,data);
	return this;
}	
@And("Enter desc as (.*)")
public AppCreateSubmit enterdesc(String data)
{   
	clearAndType(eletxtdesc,data);
	click(elebtnsub);	
	return this;
}
@And("Click Submit button")
public AppCreateSubmit clicksub()
{   
	click(elebtnsub);	
	return this;
}
}

