package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.selenium.api.base.SeleniumBase;

public class ChngeMgmtAssess extends SeleniumBase {
	
	public ChngeMgmtAssess()
	{
		PageFactory.initElements(getDriver(), this);
	}
		
	@FindBy(id="gsft_main") WebElement elebtnFrame;
	@FindBy(id="user_name") WebElement eletxtUsername;
	@FindBy(id="user_password") WebElement eletxtPassword;
	@FindBy(id="sysverb_login") WebElement elebtnLogin;
	@FindBy(id="history_tab") WebElement elebtnHistory;
	@FindBy(xpath="(//span[text()='Change Requests'])[5]") WebElement elebtnNewRec;
	@FindBy(xpath="//input[@placeholder='Search']") WebElement elebtnsearch;
	@FindBy(linkText="CHG0030010") WebElement elebtnPrbmState;
	@FindBy(id="change_request.state") WebElement elebtnchangedrop;
	@FindBy(id="sys_display.change_request.assignment_group") WebElement elebtnDesc;
	@FindBy(id="sys_display.change_request.assigned_to") WebElement elebtninsert;
	@FindBy(id="sysverb_update") WebElement elebtnsubmit;
	
//login
	
	public ChngeMgmtAssess login()
	{   
		startApp("chrome","https://dev87059.service-now.com");
		switchToFrame(0);
		clearAndType(eletxtUsername, "admin");
		clearAndType(eletxtPassword, "India@123");
		click(elebtnLogin);
		click(elebtnHistory);
		click(elebtnNewRec);
		return this;
	}
	
	public ChngeMgmtAssess search(String data)
	{
		switchToFrame(0);
		clearAndType(elebtnsearch,data);
		pressEnterKey(elebtnsearch);
		click(elebtnPrbmState);
		selectDropDownUsingText(elebtnchangedrop, "Assess");
		return this;
	}
	
	public ChngeMgmtAssess update()
	{
		clearAndType(elebtnDesc, "Software");
		pressEnterKey(elebtnDesc);
		clearAndType(elebtninsert, "ITIL User");
		pressEnterKey(elebtninsert);
		click(elebtnLogin);
		return this;
	}
}

