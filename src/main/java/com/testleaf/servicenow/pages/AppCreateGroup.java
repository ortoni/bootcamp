package com.testleaf.servicenow.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testleaf.testng.api.base.ProjectSpecificMethods;

import cucumber.api.java.en.When;

public class AppCreateGroup extends ProjectSpecificMethods{
	public AppCreateGroup() {
		PageFactory.initElements(getDriver(), this);
	}
	
@FindBy(xpath="//a[text()='Group approval']") WebElement elebtnGroup;
@FindBy(linkText="Group approval") WebElement elelnkGroup;
	

@When("Click Group Approval")
public AppCreateSubmit clickgroupapp()
{   	
	click(elelnkGroup);
	return new AppCreateSubmit();
}
	
}
